from setuptools import setup

setup(
    name='Flask_server',
    packages=['Flask_server'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)