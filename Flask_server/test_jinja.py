from flask import Flask, render_template
app = Flask(__name__)

@app.route('/hello/<int:score>')
def hello_name(score):
   return render_template('hello.html', marks = score)

@app.route('/result')
def result():
   # dict = 'phy':50,'che':60,'maths':70
   dict2 = dict([('physics', 4139), ('guido', 4127), ('jack', 4098)])
   return render_template('result.html', result = dict2)

if __name__ == '__main__':
   app.run(debug = True)
